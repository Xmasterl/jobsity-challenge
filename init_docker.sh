git clone git@bitbucket.org:Xmasterl/jobsity-challenge.git ./chatbot

cd docker;

# Build the containers by passing
# the current host user UID
# that will be set for the 'application' user
# in the web container to prevent files ownership issues
# between the host and the container.
sudo docker-compose build \
    --no-cache \
    --build-arg HOST_UID=$(id -u) \
    mysql web;

sudo docker-compose up -d;

printf "\e[93m\nSleep for 10 seconds to let the container initialize before resuming...\e[0m";
sleep 10;

# Set required directories ownership for the API.
sudo docker exec \
    -w /var/html/laravel \
    chatbot_web \
    chown -R application:application storage/;

sudo docker exec \
    -w /var/html/laravel \
    chatbot_web \
    chown -R application:application bootstrap/cache/;

cd -;

printf "\e[32m\n=== Init the Laravel app ===\n\n\e[0m";

# Copy the .env.example if not present
# (should not be present since it's not tracked by git).
if [ ! -f ./laravel/.env ]; then
    printf "Copying the '.env.example'...\n\n";
    cp ./laravel/.env.example ./laravel/.env;
else
    printf "SKIP - .env file already exists\n\n";
fi

printf "Set .env values...\n\n";
MYSQL_IP=$(sudo docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' chatbot_mysql);
sed -i -e "s/DB_HOST=127.0.0.1/DB_HOST=${MYSQL_IP}/g" ./laravel/.env;

# Install Composer packages.
printf "Install Composer packages...\n\n";
sudo docker exec \
    --user application \
    -w /var/html/laravel \
    chatbot_web \
    composer install;

# Generate dev key.
printf "\nGenerate dev key...\n\n";
sudo docker exec \
    --user application \
    -w /var/html/laravel \
    chatbot_web \
    php artisan key:generate;

printf "\nInstall DB...\n\n";
sudo docker exec \
    --user application \
    -w /var/html/laravel \
    chatbot_web \
    php artisan migrate:install;

sudo docker exec \
    --user application \
    -w /var/html/laravel \
    chatbot_web \
    php artisan migrate:fresh --seed;
