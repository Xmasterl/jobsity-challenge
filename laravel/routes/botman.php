<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('Hi', function ($bot) {
    $bot->reply('Hello!');
});
$botman->hears('Start conversation', BotManController::class.'@startConversation');

$botman->hears('I want to convert currency', BotManController::class.'@convert');

$botman->hears('Login', BotManController::class.'@login');

$botman->fallback(function($bot) {
    $bot->reply('Sorry, I did not understand that command.');
});
