<?php

namespace App\Conversations;

use Illuminate\Foundation\Inspiring;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;
use Illuminate\Support\Facades\Log;

class ConvertCurrencyConversation extends Conversation
{
    protected $from_code;
    protected $to_code;
    protected $amount;

    public function askFrom()
    {
        $this->ask('From what currency you want to convert?', function(Answer $answer) {
            // Save result
            $this->from_code = strtoupper($answer->getText());

            $this->askAmount();
        });
    }

    public function askTo()
    {
        $this->ask('To what currency you want to convert?', function(Answer $answer) {
            // Save result
            $this->to_code = strtoupper($answer->getText());

            $this->convert();
        });
    }

    public function askAmount()
    {
        $this->ask('What amount?', function(Answer $answer) {
            // Save result
            $this->amount = $answer->getText();

            $this->askTo();
        });
    }

    public function convert(){

        $url='https://www.amdoren.com/api/currency.php?api_key='.env('AMDOREN_KEY').'&from='.$this->from_code.'&to='.$this->to_code.'&amount='.$this->amount.'';
        $client = new \GuzzleHttp\Client();
        $res = $client->get($url);
        Log::info($url);
        Log::info($res->getBody());
        if($res->getStatusCode()==200){
            $result=json_decode($res->getBody(),true);
            if($result['error']==0){
                $converted_amount=$result['amount'];
                $converted_amount=number_format($converted_amount, 2);
                $this->say($this->amount." ".$this->from_code." is equal to ".$converted_amount." ".$this->to_code);
            } else{
                $this->say($result['error_message']);
            }

        } else{
            Log::error($res->getBody());
        }
    }

    /**
     * Start the conversation
     */
    public function run()
    {
        $this->askFrom();
    }
}
